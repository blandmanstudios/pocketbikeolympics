﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    public Transform cameraTarget;
    public float sSpeed= 0.125f; // higher = faster to snap, slower = smoother, 0 to 1
    public Vector3 dist;
    public Transform lookTarget;

    // Start is called before the first frame update
    void FixedUpdate()
    {
        Vector3 dPos = cameraTarget.position + dist;
        Vector3 sPos = Vector3.Lerp(transform.position, dPos, sSpeed * Time.deltaTime);
        transform.position = sPos;
        transform.LookAt(lookTarget.position);
            
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Late update because update might update player location
        //transform.position = player.position;
        //transform.rotation = player.rotation;
    }
}
