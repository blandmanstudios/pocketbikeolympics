﻿
using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class player_movement : MonoBehaviour
{
    public Rigidbody rb;
    public float forwardForce = 2000f;
    public float sidewaysForce = 500f;
    public float sideMagMax = 8000000000f;
    public Vector3 forwardVector = new Vector3(0, 0, 1);
    private float forwardController = 0f;
    private float sidewaysController = 0f;
    public float gasAccelRate = 10f;
    private float forwardSpeedFromGas = 0f;
    public float maxRot = 30f;
    public float rotScaleNum = 100f;
    public Collider rearWheelTrigger;
    public Collider frontWheelTrigger;
    private bool rearWheelEnabled = false;
    private bool frontWheelEnabled = false;
    public GameObject bike_mesh;
    public GameObject leanOrigin;

    public void EnableRearWheel()
    {
        rearWheelEnabled = true;
    }
    public void DisableRearWheel()
    {
        rearWheelEnabled = false;
    }
    public void EnableFrontWheel()
    {
        frontWheelEnabled = true;
    }
    public void DisableFrontWheel()
    {
        frontWheelEnabled = false;
    }

    public void Move(InputAction.CallbackContext context)
    {
        Vector2 axis = (Vector2)context.ReadValueAsObject();
        forwardController = axis[1];
        sidewaysController = axis[0];
    }

    // Start is called before the first frame update
    void Start()
    {
        // this executes when the game starts
        Debug.Log("Hello World");
        //rb.AddForce(0, 200, 500);
        // add some gravity make this guy fall faster
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(new Vector3(0, -30, 0));

        //Debug.Log(forwardController);
        //Debug.Log(sidewaysController);
        // Use FixedUpdate for physicsythings
        // rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        //TODO: enable disable forwardController and sidewaysController when we dont have RearWheelCollider and FrontWheelCollider overlapping with a "Floorlike" material. These are materials you can "drive" on as determined by a speciic label. This will prevent me from rocket trajectory after i go off a ramp

        if (rearWheelEnabled)
        {
            forwardSpeedFromGas += gasAccelRate * Time.deltaTime * forwardController;
        }
        // Debug.Log(forwardSpeedFromGas);

        

        Vector3 steer_vector = transform.forward;
        float steering_number = maxRot * sidewaysController * forwardSpeedFromGas / rotScaleNum;
        Quaternion myquat = Quaternion.Euler(new Vector3(0, steering_number, 0));

        // lean bike the tighter the turn. This works for now
        // TODO: Set a max, right now if you go too fast you can rotate upside down
        // TODO: Smooth out the animting for leaning in, right now it is instantaneous (on keyboard)
        //       because you instantly press a button and sidwaysController goes to -1 or 1. This
        //       make the bike snap to a leaned in position which doesnt look natural
        Vector3 bikeRot = bike_mesh.transform.rotation.eulerAngles;
        Debug.Log(steering_number);
        bikeRot.z = -1 * steering_number * 20f; // -1 to make you lean "into" the turn
        bike_mesh.transform.rotation = Quaternion.Euler(bikeRot);

        // steer_vector = myquat * steer_vector;
        if (frontWheelEnabled)
        {
            transform.rotation *= myquat;
        }
        //Debug.Log();


        rb.MovePosition(transform.position + steer_vector * forwardSpeedFromGas * Time.deltaTime);


        if (forwardController > 0)
        {
            // rb.AddForce(transform.forward * forwardForce * Time.deltaTime);
            // rb.AddForceAtPosition(transform.forward * forwardForce * Time.deltaTime, -transform.forward);
            // rb.AddForce(transform.forward * forwardForce * Time.deltaTime);
            //rb.AddForceAtPosition(transform.forward * forwardForce * Time.deltaTime, transform.position - transform.forward * 2f);
            


        }
        if (forwardController < 0)
        {
            // rb.AddForce(-transform.forward * forwardForce * Time.deltaTime);
            // rb.AddForceAtPosition(-transform.forward * forwardForce * Time.deltaTime, -transform.forward);
            // rb.AddForce(-transform.forward * forwardForce * Time.deltaTime);
            //rb.AddForceAtPosition(-transform.forward * forwardForce * Time.deltaTime, transform.position - transform.forward * 2f);
        }
        if (sidewaysController > 0)
        {
            //rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
            //rb.AddTorque(transform.up * sidewaysForce * Time.deltaTime);
            //rb.AddForce(transform.right * sidewaysForce * Time.deltaTime);
            //float sideMagnitude = Math.Min(sideMagMax, sidewaysForce * Time.deltaTime * Vector3.Dot(rb.velocity, transform.forward));
            //Vector3 turnVector = transform.right;
            //rb.AddForceAtPosition(turnVector * sideMagnitude, transform.position + (transform.forward * 2f));
        }
        if (sidewaysController < 0)
        {
            //rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
            //rb.AddTorque(-transform.up * sidewaysForce * Time.deltaTime);
            //rb.AddForce(-transform.right * sidewaysForce * Time.deltaTime);
            //float sideMagnitude = Math.Min(sideMagMax, sidewaysForce * Time.deltaTime * Vector3.Dot(rb.velocity, transform.forward));
            //Vector3 turnVector = transform.right;
            //rb.AddForceAtPosition(-1 * turnVector * sideMagnitude, transform.position + (transform.forward * 2f));
        }

        if (rb.position.y < -2f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}
