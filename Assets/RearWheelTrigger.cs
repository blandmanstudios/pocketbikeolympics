﻿using UnityEngine;

public class RearWheelTrigger : MonoBehaviour
{
    public player_movement playerMove;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "FloorLike")
        {
            playerMove.EnableRearWheel();
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "FloorLike")
        {
            playerMove.DisableRearWheel();
        }
    }
}
