﻿using UnityEngine;

public class FrontWheelTrigger : MonoBehaviour
{
    public player_movement playerMove;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "FloorLike")
        {
            playerMove.EnableFrontWheel();
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "FloorLike")
        {
            playerMove.DisableFrontWheel();
        }
    }
}
